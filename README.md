[![pipeline status](https://gitlab.com/odg/metasbc/badges/master/pipeline.svg)](https://gitlab.com/odg/metasbc/commits/master)

[**MetaSBC Website**](https://odg.gitlab.io/metasbc)

---

# MetaSBC

This is a project to collect as much data as possible, in a consistent and
logical way, about Single Board Computers. This includes physical and
electrical specifications, information about the SoC they use, how usable they
are with free software, and so on. Also there is a small site which displays
the data in interactive tables, for easy comparison.

This project's website is rebuilt by the CI whenever commits are pushed to
GitLab, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml), and
hosted on GitLab Pages.

## Building

The website uses Hugo, a static site generator. To build the site locally for
testing, you need to:

1. Fork, clone or download this project
2. [Install](https://gohugo.io/overview/installing) Hugo
3. Run `hugo server`
4. Visit <localhost:1313/metasbc> to see the site

See the [Hugo documentation](https://gohugo.io/overview/introduction) for more
details.

The HTML tables are generated with the Python script `gen-table.py` in `/data`.

## Data

The data is in the `/data` subdirectory in this repository. It is formatted in
the ODS (Open Document Spreadsheet) format, to be edited with spreadsheet
software, such as LibreOffice Calc. To contribute (by adding an SBC, adding
more information, or correcting an error), just fork the repository, edit the
spreadsheet(s), and submit a Merge Request.

Run these commands to be able to view useful diffs of the spreadsheets, for
example when doing `git diff` or `git log -p`:
```
git config --global diff.ods.textconv "unoconv --stdout -f csv"
git config --global diff.ods.cachetextconv true
```

The other subdirectories contain templates and Markdown files for the
Hugo-generated website. The files from external dependencies are in
`/static/external`, which include [DataTables](https://www.datatables.net) and
[jQuery](https://jquery.com).

## License

DataTables and jQuery are released under an MIT license.

The logo used is created by Oriza Creativa from the Noun Project, and released
under the Creative Commons Attribution License.

All code and data that is part of MetaSBC is released under the terms of the
GNU Affero General Public License, version 3 or later.
