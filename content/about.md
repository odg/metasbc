+++
title = "About"
+++

# About

You can see the all code and data for this site on GitLab
[here](https://gitlab.com/odg/MetaSBC).

## The Data

All data used is collected from public sources, such as manufacturer's official
websites. I've tried to list SBCs which are at least somewhat popular,
focussing on SBCs which are usable with free software, have a focus on freedom
and openness, and are at least still available to purchase. Listing boards on
this site **does not mean they are approved or recommended by the author**,
it's just an attempt to have as complete a list as possible while also being
easy to navigate. If you see anything inaccurate or there's an SBC you'd like
to see added, feel free to submit a merge request.

### What is an SBC?

The definition of what an SBC is can get somwhat blurry. But I tend to stick to
the rule that everything needed to use the board comes on the board itself
(they are "Single Board" after all), except essential peripherals like a
keyboard and monitor. This means the CPU is built into an SoC which is soldered
on to the board (usually RAM is soldered on too), and they can be run straight
from DC power (usually via USB). The boards should be a maximum size of about
4"x4" (101.6mm x 101.6mm), the size of the Intel NUC boards. Much bigger than
that and you start to get into the territory of socketed motherboards (MiniITX
is 170mm x 170mm), which are too numerous to list here and don't really qualify
as SBCs.

### Acronyms

Common acronyms used on this site.

 * SBC - Single Board Computer
 * SoC - System on Chip
 * CPU - Central Processing Unit
 * GPU - Graphics Processing Unit
 * ISA - Instruction Set Architecture
 * I2C - Inter-integrated circuit
 * SPI - Serial Peripheral Interface
 * UART - Universal Asynchronous Receiver-Transmitter
 * GPIO - General Purpose Input and Output

## Technical

### How It Works

The raw data for the tables displayed on this site is preprocessed with a
script using the [Pandas](https://pandas.pydata.org) and
[Beautiful Soup](https://www.crummy.com/software/BeautifulSoup) Python modules,
which generate and modify plain HTML table code.

The site is then managed using [Hugo](https://gohugo.io), a static site
generator written in Go, which has a powerful templating system. The generated
tables are combined with templates to generate the site's complete code, which
is hosted by [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages).
GitLab's continuous integration system is used to automatically run Hugo to
regenerate the site code whenever the raw data is modified.

Once loaded by your browser, the site loads
[DataTables](https://www.datatables.net), a jQuery plug-in, to add extra
functionality to the tables such as sorting and searching.

### Licensing

All code, whether running on the server, on my computer to process the data,
and the Javascript which runs in your browser, is
[free software](https://www.gnu.org/philosophy/free-sw.html).

The raw data, website code, and scripts which are part of MetaSBC are released
under the GNU Affero General Public License version 3 or later. The other
projects used by MetaSBC are released separately under their own licenses, see
their websites linked above for more information.
