+++
title = "Javascript Weblabels"
+++

<table id="jslicense-labels1">
 <tr>
  <td><a href="/external/jquery-3.4.1.min.js">jquery-3.4.1.min.js</a></td>
  <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
  <td><a href="/external/jquery-2.1.1.js">jquery-3.4.1.js</a></td>
 </tr>
 <tr>
  <td><a href="/external/datatables.min.js">datatables.min.js</a></td>
  <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
  <td><a href="/external/datatables.js">datatables.js</a></td>
 </tr>
</table>
