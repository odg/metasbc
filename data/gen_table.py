#!/usr/bin/python3
"""
This module generates HTML tables for the MetaSBC project, based on data in
spreadsheets. The data is also merged to produce a large table with all the
data combined (the 'parent' table). The pandas library is used to do SQL-style
merges and generate HTML, which is further modified using BeautifulSoup.

Copyright (C) 2019 Oliver Galvin <odg@riseup.net>
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from prettify_html import prettify

parent = "SBC"
children = ["SoC", "uArch"]
tables = [parent] + children

na = "N/A"

def get_wiki_link(article):
    """ For the given Wikipedia article name, return its URL. """
    lang = "en"
    suffix = str(article).replace(" ", "_")
    if not suffix:
        return ""
    suffix = suffix[0].upper() + suffix[1:]
    return "https://{}.wikipedia.org/wiki/{}".format(lang, suffix)

def gen_html(tab, name):
    """ Generate and process HTML from the given pandas DataFrame. """
    html = tab.to_html(index=False, render_links=True, justify="unset",
                       classes=["cell-border", "hover", "order-column",
                                "row-border", "stripe"],
                       na_rep=na, table_id="main-table")
    soup = BeautifulSoup(html, "html.parser")
    types = list(tab.dtypes)
    for row in soup.tbody("tr"):
        for i, cell in enumerate(row("td")):
            col = list(tab.iloc[:, i].values)
            if types[i] == np.dtype('bool') or \
               (types[i] != np.dtype('O') and 0 in col):
                cell["class"] = "false" if cell.string in (na, "0", "False") \
                                        else "true"
    del soup.table["border"]
    for section in soup.thead.tr("th"):
        del section["halign"]
        section["class"] = "section"
    for link in soup("a"):
        link.string = "Link"
    with open("{}.html".format(name), "w") as output:
        output.write(prettify(soup, indent_top=3))


print("Reading the data...")
table = {}
for data in Path.cwd().glob("*.ods"):
    table[data.stem] = pd.read_excel(
        data, engine="odf", header=[0, 1],
        true_values=["Yes", "yes", "True", "true"],
        false_values=["No", "no", "False", "false"],
        converters={("Links", "Wikipedia Article"): get_wiki_link})


print("Merging the tables...")
parent_table = table[parent]
for child in children:
    parent_table = parent_table.merge(table[child], how="left",
                                      on=[table[child].columns[0]],
                                      suffixes=(" - {}".format(parent),
                                                " - {}".format(child)))

print("Generating HTML...")
gen_html(parent_table, parent)
for child in children:
    gen_html(table[child], child)
