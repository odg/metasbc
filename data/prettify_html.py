""" Pretty print HTML from a BeautifulSoup object. """

from os import linesep
from bs4 import BeautifulSoup, element

def prettify(soup, indent_top=0, indent_str=" ", indent_plain=False):
    """
    Recursive function which returns a tag's string representation, with the
    given indent level, and then calls itself on each child tag.
    """
    res = ""
    prefix = linesep + indent_top * indent_str
    for tag in soup.children:
        if isinstance(tag, element.Tag):
            res += prefix + "<" + tag.name
            if tag.attrs:
                for attr in sorted(tag.attrs):
                    val = tag.attrs[attr]
                    val = " ".join(sorted(val)) \
                          if isinstance(val, list) else str(val)
                    res += " {}=\"{}\"".format(attr, val)
            res += ">" + prettify(tag,
                                  indent_top=indent_top + 1,
                                  indent_str=indent_str,
                                  indent_plain=indent_plain)
            if not tag.can_be_empty_element:
                if res[-1] == ">" or indent_plain:
                    res += prefix
                res += "</{}>".format(tag.name)
        elif tag != linesep:
            if not isinstance(tag, element.NavigableString) or indent_plain:
                res += prefix
            res += str(tag)
    return res

def prettify_raw(html, indent_top=2, indent_str=" ", indent_plain=False):
    """ Parse a raw HTML string before pretty printing it. """
    soup = BeautifulSoup(html, "html.parser")
    return prettify(soup, indent_top, indent_str, indent_plain)
